package com.android.one;

import android.test.AndroidTestCase;

public class UtilTest extends AndroidTestCase {

	public void testGetWeekDaysCount() {
		// change below statement accordingly
		assertEquals(
				14,
				Util.getWeekDaysCount(System.currentTimeMillis()
						- (Util.ONE_DAY * 20)));
	}

	public void testGetDaysCount() {
		assertEquals(0, Util.getDaysCount(System.currentTimeMillis()));
		assertEquals(1,
				Util.getDaysCount(System.currentTimeMillis() - Util.ONE_DAY));
	}

	public void testGetWeeksCount() {
		assertEquals(0, Util.getWeeksCount(System.currentTimeMillis()));
		assertEquals(
				1,
				Util.getWeeksCount(System.currentTimeMillis()
						- (Util.ONE_DAY * 10)));

		assertEquals(
				1,
				Util.getWeeksCount(System.currentTimeMillis()
						- (Util.ONE_DAY * 7)));
		assertEquals(
				0,
				Util.getWeeksCount(System.currentTimeMillis()
						- (Util.ONE_DAY * 6)));

	}
}
