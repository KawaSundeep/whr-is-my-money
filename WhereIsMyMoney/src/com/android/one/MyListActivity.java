package com.android.one;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MyListActivity extends Activity {
	private DataHelper dataHelper;
	private List<Item> result;
	private Spinner account_spnr;
	private TextView spent;
	private ArrayAdapter<String> adapter_spnr;
	private ListView listView;
	private ListAdapter adapter = null;
	public static String account = "";
	private Context context = this;

	private final String PROJ_PREF = "WPref";
	private final String ACCOUNT = "accountName";
	String selectedAccount;
	SharedPreferences pref;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mylistlayout);
		account_spnr = (Spinner) findViewById(R.id.account_spnr2);
		spent = (TextView) findViewById(R.id.listCount_tv);
		listView = (ListView) findViewById(R.id.myList_lv);
		dataHelper = new DataHelper(this);

		pref = getSharedPreferences(PROJ_PREF, 0);
		if (pref.getBoolean(Constants.IS_LIST_MNGD, true)) {
			Toast.makeText(context, "Long press an Item to edit",
					Toast.LENGTH_LONG).show();
		}
		selectedAccount = pref.getString(ACCOUNT, null);

		List<String> oldAccounts = new ArrayList<String>();
		oldAccounts = dataHelper.getAccounts();

		int selectedIndex = oldAccounts.indexOf(selectedAccount);
		if (selectedIndex > 0) {
			Collections.swap(oldAccounts, selectedIndex, 0);
		}

		adapter_spnr = new ArrayAdapter<String>(getApplicationContext(),
				android.R.layout.simple_spinner_item, oldAccounts);
		account_spnr.setAdapter(adapter_spnr);

		account_spnr.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				selectedAccount = parent.getItemAtPosition(position).toString();

				Editor edit = pref.edit();
				edit.putString(ACCOUNT, selectedAccount);
				edit.commit();

				result = dataHelper.getAllItems(selectedAccount);

				adapter = new MyBaseAdapterFull(getApplicationContext(), result);
				listView.setAdapter(adapter);

				setCount();
				//
				// Toast.makeText(getApplicationContext(), accountSelected,
				// Toast.LENGTH_LONG).show();

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		result = new ArrayList<Item>();
		result = dataHelper.getAllItems(selectedAccount);

		setCount();

		adapter = new MyBaseAdapterFull(getApplicationContext(), result);
		listView.setAdapter(adapter);
		registerForContextMenu(listView);
		/*
		 * listView.setOnItemClickListener(new OnItemClickListener() {
		 * 
		 * @Override public void onItemClick(AdapterView<?> a, View v, int
		 * position, long id) { Object o = listView.getItemAtPosition(position);
		 * Item item = (Item) o; Toast.makeText(getApplicationContext(),
		 * item.getId() + "" + item.getName(), Toast.LENGTH_SHORT) .show(); }
		 * });
		 */
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater mInflater = new MenuInflater(getApplicationContext());
		mInflater.inflate(R.menu.listitemmenu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		Item selectedItem = null;
		if (pref.getBoolean(Constants.IS_LIST_MNGD, true)) {
			Editor edit = pref.edit();
			edit.putBoolean(Constants.IS_LIST_MNGD, false);
			edit.commit();
		}
		switch (item.getItemId()) {
		case R.id.edit:
			selectedItem = dataHelper.getItemById(info.id);

			long id = selectedItem.getId();
			String name = selectedItem.getName();
			double value = selectedItem.getValue();

			Log.i("selected item:", name + " " + value);

			Bundle bundle = new Bundle();
			bundle.putLong("id", id);
			bundle.putString("name", name);
			bundle.putDouble("value", value);
			bundle.putString("account", selectedAccount);
			Intent intent = new Intent(getApplicationContext(),
					WhereIsMyMoneyActivity.class);
			intent.putExtras(bundle);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			Log.i("exe", "after call");

			return true;
		case R.id.delete:
			selectedItem = dataHelper.getItemById((int) info.id);

			Log.i("selected item:",
					selectedItem.getName() + " " + selectedItem.getValue());

			Log.i("deleted", "" + dataHelper.deleteItemById(info.id));

			result = dataHelper.getAllItems(selectedAccount);
			Collections.reverse(result);
			adapter = new MyBaseAdapterFull(getApplicationContext(), result);
			listView.setAdapter(adapter);

			Toast.makeText(getApplicationContext(), "deleted",
					Toast.LENGTH_SHORT);

			return true;
		default:
			return super.onContextItemSelected(item);
		}

	}

	private void setCount() {
		spent.setText(Constants.TOTAL_AMOUNT
				+ dataHelper.getAmountCount(selectedAccount));
	}

}
