package com.android.one;

public class Constants {
	public static final String DBName = "wallet";

	public static final String ITEM_TABLE = "item";
	public static final String ITEM_ID = "_id";
	public static final String ITEM_NAME = "name";
	public static final String ITEM_VALUE = "value";
	public static final String ITEM_DATE = "date";
	public static final String ITEM_ACCOUNT = "account";

	public static final String ACCOUNT_TABLE = "account";
	public static final String ACCOUNT_ID = "_id";
	public static final String ACCOUNT_NAME = "name";

	public static final String WORD_TABLE = "word";
	public static final String WORD_ID = "_id";
	public static final String WORD_NAME = "name";

	public static final String TASK_TABLE = "task";
	public static final String TASK_ID = "taskId";
	// Task table includes all columns and below columns
	public static final String TASK_REPEAT_ID = "repeatDays";
	public static final String TASK_INSERTED_COUNT = "insertedCount";

	public static final String DATAHELPER_TAG = "DataHelper";
	public static final String WIM_TAG = "WhereIsMyMoneyActivity";
	public static final String ACCOUNTS_TAG = "AccountsActivity";
	public static final String MYLIST_TAG = "DataHelper";

	public static final String PROJ_PREF = "WPref";
	public static final String ACCOUNT = "accountName";
	public static final String IS_FIRST = "accountMenu";
	public static final String IS_LIST_MNGD = "listMngd";

	public static final String TODAY_AMOUNT = "Amount spent today: ";
	public static final String TOTAL_AMOUNT = "Total amount spent: ";

	public static final String EVERY = "every";
	public static final String EVERYDAY = "every day";
	public static final String MON_FRI = "mon - fri";

	public static final CharSequence SAVED = "Saved";
	public static final CharSequence DIALOG_REPEAT_TITLE = "When shall I repeat?";

}