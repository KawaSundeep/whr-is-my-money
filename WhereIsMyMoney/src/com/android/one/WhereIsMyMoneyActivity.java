package com.android.one;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class WhereIsMyMoneyActivity extends Activity {
	/** Called when the activity is first created. */

	private DataHelper dataHelper;

	private TextView spent_tv;
	private AutoCompleteTextView autoComplete_tv;
	private EditText value_et;
	private Button save_btn, repeat_btn, viewitems_btn;
	public Spinner account_spnr;
	public ListView recentList;

	private ArrayAdapter<String> adapter_autoComplete;
	private ArrayAdapter<String> adapter_spinner;
	private ListAdapter adapter_recentList;

	public long id = -1;
	public String name = null;
	public double value = 0.0;
	public int repeatId = -1;
	public boolean fromRestart = false;
	public Context context = this;

	private SharedPreferences pref;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(Constants.WIM_TAG, "create");
		new AddRepeatItemsAsync(context).execute();
		fromRestart = false;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		pref = getSharedPreferences(Constants.PROJ_PREF, 0);

		if (pref.getBoolean(Constants.IS_FIRST, true)) {
			Toast.makeText(context, "Select menu to manage account",
					Toast.LENGTH_LONG).show();
		}

		autoComplete_tv = (AutoCompleteTextView) findViewById(R.id.autoComplete_tv);
		value_et = (EditText) findViewById(R.id.value_et);

		repeat_btn = (Button) findViewById(R.id.repeat_btn);
		save_btn = (Button) findViewById(R.id.save_btn);
		viewitems_btn = (Button) findViewById(R.id.view_items);
		recentList = (ListView) findViewById(R.id.recentList_lv);
		account_spnr = (Spinner) findViewById(R.id.account_spnr1);

		spent_tv = (TextView) findViewById(R.id.spent_tv);

		dataHelper = new DataHelper(context);

		adapter_autoComplete = new ArrayAdapter<String>(context,
				R.layout.autocompleterow, dataHelper.getWords());
		autoComplete_tv.setAdapter(adapter_autoComplete);

		// Bundle editBundle = getIntent().getExtras();
		// if(editBundle!= null){
		// editBundle.getLong("id");
		// editBundle.getString("name");
		// editBundle.getDouble("value");
		// selectedAccount = editBundle.getString("account");
		// }

		account_spnr.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				setPrefAccount(parent.getItemAtPosition(position).toString());
				// Toast.makeText(getApplicationContext(), selectedAccount,
				// Toast.LENGTH_LONG).show();
				setRecentList(getPrefAccount());
				setCount();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		repeat_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showDialog(2);
			}
		});

		save_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				name = autoComplete_tv.getText().toString();
				try {
					if (name.length() == 0) {
						throw new Exception("Item name is Empty");
					}

					value = Double.parseDouble(value_et.getText().toString());
					value += 0.001;
					StringBuilder s = new StringBuilder(value + "");

					value = Double.parseDouble(s.substring(0,
							s.indexOf(".") + 3));

					Item item = new Item(id, name, value, new Date(System
							.currentTimeMillis()), getPrefAccount());

					item = dataHelper.insertItem(item);
					if (repeatId > -1) {
						Task task = null;
						switch (repeatId) {
						case 0:// everyday
							task = new Task(item, repeatId, 0);
							break;
						case 1:// mon-fri
							task = new Task(item, repeatId, 1);
							break;
						case 2:// weekly
							task = new Task(item, repeatId, 2);
							break;
						}

						dataHelper.addTask(task);
					}

					Toast.makeText(getApplicationContext(), Constants.SAVED,
							Toast.LENGTH_SHORT).show();
					setRecentList(getPrefAccount());
					setCount();
					clearFields();
					if (dataHelper.addWord(name) > 0) {
						adapter_autoComplete.add(name);
					}
				} catch (NumberFormatException d) {
					Toast.makeText(getApplicationContext(),
							"Amount is unreadable!", Toast.LENGTH_LONG).show();
					value_et.setFocusable(true);
				} catch (Exception e) {
					Toast.makeText(getApplicationContext(),
							"" + e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}
		});

		viewitems_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),
						MyListActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.account_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (pref.getBoolean(Constants.IS_FIRST, true)) {
			Editor edit = pref.edit();
			edit.putBoolean(Constants.IS_FIRST, false);
			edit.commit();
		}
		switch (item.getItemId()) {

		case R.id.createAccount_mi:
			setPrefAccount(null);
			Intent intent = new Intent(context, AccountActivity.class);
			startActivity(intent);
			return true;
		case R.id.deleteAccount_mi:
			showDialog(1);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected Dialog onCreateDialog(int i) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		switch (i) {
		case 1:
			builder.setMessage(
					"Are you sure you want to delete '" + getPrefAccount()
							+ "' account ?\n" + " (cannot be undone)")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dataHelper.deleteAccount(getPrefAccount());

									List<String> remain = dataHelper
											.getAccounts();

									if (remain.size() > 0) {
										setPrefAccount(remain.get(0));
										setAccountsSpinner(remain);
										setRecentList(getPrefAccount());
										setCount();
									} else {
										setPrefAccount(null);
										finish();
										Intent intent = new Intent(context,
												AccountActivity.class);
										intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
										startActivity(intent);
									}
									removeDialog(1);
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									removeDialog(1);
								}
							});

			break;
		case 2:
			String week = "every" + " " + Util.getWeekName();
			final CharSequence items[] = { Constants.EVERYDAY,
					Constants.MON_FRI, week };
			builder.setTitle(Constants.DIALOG_REPEAT_TITLE);
			builder.setSingleChoiceItems(items, -1,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int item) {
							repeatId = item;
							Toast.makeText(getApplicationContext(),
									items[item], Toast.LENGTH_SHORT).show();
						}
					}).setPositiveButton("Confirm", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					Toast.makeText(getApplicationContext(), "confirm",
							Toast.LENGTH_SHORT).show();
					removeDialog(2);
				}
			}).setNegativeButton("Cancel", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					repeatId = -1;
					Toast.makeText(getApplicationContext(), "cancel",
							Toast.LENGTH_SHORT).show();
					removeDialog(2);
				}
			}).setCancelable(false);
			break;
		}

		return builder.create();
	}

	public void onStart() {
		super.onStart();
		Log.i(Constants.WIM_TAG, "start");
		Intent intent = getIntent();
		Bundle b = intent.getExtras();
		if (b != null && !fromRestart) {
			id = b.getLong("id");
			Log.i("check", "setting values");
			autoComplete_tv.setText(b.getString("name"));
			value_et.setText(b.getDouble("value") + "");
			b.clear();
			intent = null;
		}

	}

	public void onRestart() {
		Log.i(Constants.WIM_TAG, "restart");
		super.onRestart();
		fromRestart = true;
	}

	public void onResume() {
		Log.i(Constants.WIM_TAG, "resume");
		super.onResume();

		List<String> oldAccounts = new ArrayList<String>();
		oldAccounts = dataHelper.getAccounts();

		int selectedIndex = oldAccounts.indexOf(getPrefAccount());
		if (selectedIndex > 0) {
			Collections.swap(oldAccounts, selectedIndex, 0);
		}
		Log.i(Constants.WIM_TAG, "1");
		setAccountsSpinner(oldAccounts);
		Log.i(Constants.WIM_TAG, "2");
		setRecentList(oldAccounts.get(0));
		Log.i(Constants.WIM_TAG, "3");
		setCount();
		Log.i(Constants.WIM_TAG, "4");
	}

	public void onStop() {
		Log.i(Constants.WIM_TAG, "stop");
		super.onStop();
	}

	public void onPause() {
		Log.i(Constants.WIM_TAG, "pause");
		super.onPause();
	}

	public void onDestroy() {
		Log.i(Constants.WIM_TAG, "destroy");
		super.onDestroy();
	}

	public void setRecentList(String account) {
		adapter_recentList = new MyBaseAdapter(context,
				dataHelper.getRecentItems(account));
		recentList.setAdapter(adapter_recentList);
	}

	public void setAccountsSpinner(List<String> oldAccounts) {
		adapter_spinner = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, oldAccounts);
		account_spnr.setAdapter(adapter_spinner);
	}

	private void setCount() {
		spent_tv.setText(Constants.TODAY_AMOUNT
				+ dataHelper.getTodayAmountCount(getPrefAccount()));
	}

	public void clearFields() {
		id = -1;
		autoComplete_tv.setText("");
		value_et.setText("");
		repeatId = -1;
	}

	public void setPrefAccount(String account) {
		Editor edit = pref.edit();
		edit.putString(Constants.ACCOUNT, account);
		edit.commit();
	}

	public String getPrefAccount() {
		return pref.getString(Constants.ACCOUNT, null);
	}

}