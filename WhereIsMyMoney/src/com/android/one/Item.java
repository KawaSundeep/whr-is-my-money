package com.android.one;

import java.util.Date;

public class Item {

	public long id = -1;
	public String name = null;
	public double value = 0;
	public Date dateTime = null;
	public String accountName = null;

	public Item(long id, String name, double value, Date dateTime,
			String accountName) {
		this.id = id;
		this.name = name;
		this.value = value;
		this.dateTime = dateTime;
		this.accountName = accountName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

}
