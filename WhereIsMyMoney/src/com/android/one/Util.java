package com.android.one;

import java.util.Date;

public class Util {
	public static final long ONE_DAY = 1000 * 60 * 60 * 24;

	public static String getWeekName() {
		switch (new Date(System.currentTimeMillis()).getDay()) {
		case 0:
			return " Sunday";
		case 1:
			return " Monday";
		case 2:
			return " Tuesday";
		case 3:
			return " Wednesday";
		case 4:
			return " Thursday";
		case 5:
			return " Friday";
		case 6:
			return " Saturday";
		}
		return "";
	}

	// returns no of working days (Monday - Friday) passed from the given date
	// to the current system date
	public static int getWeekDaysCount(long pastDate) {
		if (pastDate == 0) {
			return (int) pastDate;
		}

		int count = 0;
		int deltaDays = getDaysCount(pastDate);

		if (deltaDays <= 0)
			return count;

		count = (deltaDays / 7) * 5;
		int leftDays = deltaDays % 7;

		for (int i = 0; i < leftDays; i++) {
			pastDate = pastDate + ONE_DAY;
			switch (new Date(pastDate).getDay()) {
			case 0:// catch Sunday
				break;
			case 6:// catch Saturday
				break;
			default:// Monday - FriDay
				count++;
			}
		}
		return count;
	}

	// returns no of days passed from the given date to the current system date
	public static int getDaysCount(long pastDate) {
		if (pastDate == 0) {
			return 0;
		}
		long deltaMSeconds = System.currentTimeMillis() - pastDate;
		return Math.abs((int) (deltaMSeconds / (ONE_DAY)));
	}

	// returns no of weeks passed from the given date to the current system date
	public static int getWeeksCount(long pastDate) {
		if (pastDate == 0) {
			return 0;
		}
		return getDaysCount(pastDate) / 7;
	}
}
