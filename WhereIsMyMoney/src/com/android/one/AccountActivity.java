package com.android.one;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class AccountActivity extends Activity {
	private Context context = this;
	private EditText newAccount;
	private ListView accountsList;
	private Button create_btn;
	private ArrayAdapter<String> accountsAdapter;

	private DataHelper dataHelper;
	private List<String> oldAccounts;
	SharedPreferences pref;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		pref = getSharedPreferences(Constants.PROJ_PREF, 0);
		String hasAccount = pref.getString(Constants.ACCOUNT, null);
		if (hasAccount != null) {
			finish();
			Intent intent = new Intent(context, WhereIsMyMoneyActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		} else {
			Log.i(Constants.ACCOUNTS_TAG, " No accounts found");
			setContentView(R.layout.account);
			dataHelper = new DataHelper(context);
			oldAccounts = new ArrayList<String>();
			oldAccounts = dataHelper.getAccounts();

			newAccount = (EditText) findViewById(R.id.newAccount_et);
			accountsList = (ListView) findViewById(R.id.accountsList_lv);
			create_btn = (Button) findViewById(R.id.create_btn);

			accountsAdapter = new ArrayAdapter<String>(context,
					android.R.layout.simple_list_item_1, oldAccounts);
			accountsList.setAdapter(accountsAdapter);

			create_btn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String newName = newAccount.getText().toString()
							.toLowerCase();
					if (!oldAccounts.contains(newName) && newName.length() > 0) {
						dataHelper.addAccount(newName);

						Editor edit = pref.edit();
						Log.i("putting", "" + newName);
						edit.putString(Constants.ACCOUNT, newName);
						edit.commit();

						finish();
						Intent intent = new Intent(context,
								WhereIsMyMoneyActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);

					} else {
						Toast.makeText(context,
								"you have an account with this name",
								Toast.LENGTH_LONG).show();
					}

				}
			});

			if (oldAccounts.size() > 0) {
				Editor edit = pref.edit();
				Log.i("putting", "odAccount");
				edit.putString(Constants.ACCOUNT, oldAccounts.get(0));
				edit.commit();
			} else {
				Toast.makeText(context, "Create account with a name",
						Toast.LENGTH_LONG).show();
				Toast.makeText(context,
						"it can be managed for a month or long time",
						Toast.LENGTH_LONG).show();
			}
		}
	}
}
