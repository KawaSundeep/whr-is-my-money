package com.android.one;

import java.util.Date;
import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class AddRepeatItemsAsync extends AsyncTask<Void, Void, Void> {
	Context context;
	DataHelper helper;
	static final long ONE_DAY = 1000 * 60 * 60 * 24;

	public AddRepeatItemsAsync(Context context) {
		this.context = context;
	}

	@Override
	protected Void doInBackground(Void... arg0) {
		Log.i("Async", "doInbackground");
		helper = new DataHelper(context);
		List<Task> tasks = helper.getAllTasks();
		if (tasks.size() > 0) {
			Log.i("Async", "size:" + tasks.size());
			for (Task task : tasks) {
				Item item;
				int diff;
				switch (task.getRepeatId()) {
				case 0:
					// every day;
					item = task.getItem();
					diff = Util.getDaysCount(item.getDateTime().getTime());
					addItems(task, item, diff);
					break;
				case 1:
					// mon-fri
					item = task.getItem();
					diff = Util.getWeekDaysCount(item.getDateTime().getTime());
					addItems(task, item, diff);
					break;
				case 2:
					// every week;
					item = task.getItem();
					diff = (int) (new Date().getTime() - item.getDateTime()
							.getTime()) / (1000 * 60 * 60 * 24 * 7);
					addItems(task, item, diff);
					break;
				}
			}
		}
		Log.i("Async", "size:" + tasks.size());
		return null;
	}

	private void addItems(Task task, Item item, int diff) {
		int sqlCount;
		sqlCount = task.getInsertedCount();
		if (sqlCount > 0) {
			diff = diff - sqlCount;
		}
		for (int i = 0; i < diff; i++) {
			item.setId(-1);
			item.setDateTime(new Date(System.currentTimeMillis()));
			helper.insertItem(item);
		}
		task.setInsertedCount(diff + sqlCount);
		helper.addTask(task);
	}

	@Override
	protected void onPostExecute(Void result) {
		Log.i("Async", "post");
		super.onPostExecute(result);
	}
}
