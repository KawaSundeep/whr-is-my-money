package com.android.one;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataHelper extends SQLiteOpenHelper {
	SQLiteDatabase db;

	public DataHelper(Context context) {
		super(context, Constants.DBName, null, 1);

	}

	@Override
	public void onCreate(SQLiteDatabase SQLDb) {
		try {

			String sql = "CREATE TABLE " + Constants.ACCOUNT_TABLE + " ( "
					+ Constants.ACCOUNT_ID + " INTEGER, "
					+ Constants.ACCOUNT_NAME + " VARCHAR(255) PRIMARY KEY)";
			SQLDb.execSQL(sql);
			Log.e(Constants.DATAHELPER_TAG, Constants.ACCOUNT_TABLE
					+ " table created ");

			sql = "CREATE TABLE " + Constants.WORD_TABLE + " ( "
					+ Constants.WORD_ID + " INTEGER, " + Constants.WORD_NAME
					+ " VARCHAR(255) PRIMARY KEY)";
			SQLDb.execSQL(sql);
			Log.e(Constants.DATAHELPER_TAG, Constants.WORD_TABLE
					+ " table created ");

			sql = "CREATE TABLE " + Constants.ITEM_TABLE + " ( "
					+ Constants.ITEM_ID
					+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ Constants.ITEM_NAME + " VARCHAR(255), "
					+ Constants.ITEM_VALUE + " DOUBLE, " + Constants.ITEM_DATE
					+ " LONG, " + Constants.ITEM_ACCOUNT + " VARCHAR(255))";
			SQLDb.execSQL(sql);
			Log.e(Constants.DATAHELPER_TAG, Constants.ITEM_TABLE
					+ " table created ");

			sql = "CREATE TABLE " + Constants.TASK_TABLE + " ( "
					+ Constants.TASK_ID
					+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ Constants.ITEM_ID + " INTEGER , " + Constants.ITEM_NAME
					+ " VARCHAR(255), " + Constants.ITEM_VALUE + " DOUBLE, "
					+ Constants.ITEM_DATE + " LONG, " + Constants.ITEM_ACCOUNT
					+ " VARCHAR(255), " + Constants.TASK_REPEAT_ID
					+ " INTEGER, " + Constants.TASK_INSERTED_COUNT
					+ " INTEGER)";
			SQLDb.execSQL(sql);
			Log.e(Constants.DATAHELPER_TAG, Constants.TASK_TABLE
					+ " table created ");

		} catch (SQLException SQLEx) {
			Log.e(Constants.DATAHELPER_TAG,
					"Caught Exception " + SQLEx.getMessage());
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String sql = "DROP TABLE IF EXISTS " + Constants.ACCOUNT_TABLE;
		db.execSQL(sql);
		sql = "DROP TABLE IF EXISTS " + Constants.ITEM_DATE;
		db.execSQL(sql);
		sql = "DROP TABLE IF EXISTS " + Constants.WORD_TABLE;
		db.execSQL(sql);
		sql = "DROP TABLE IF EXISTS " + Constants.TASK_TABLE;
		db.execSQL(sql);

		onCreate(db);
	}

	public Item insertItem(Item item) {
		ContentValues values = new ContentValues();
		values.put(Constants.ITEM_NAME, item.getName());
		values.put(Constants.ITEM_VALUE, item.getValue());
		values.put(Constants.ITEM_DATE, System.currentTimeMillis());
		values.put(Constants.ITEM_ACCOUNT, item.getAccountName());
		long id = 0;
		db = getWritableDatabase();
		try {
			if (item.getId() > 0) {
				id = db.update(Constants.ITEM_TABLE, values, Constants.ITEM_ID
						+ "=?", new String[] { String.valueOf(item.getId()) });
			} else {
				id = db.insertOrThrow(Constants.ITEM_TABLE, null, values);
			}
		} catch (SQLException ex) {
		} finally {
			db.close();
		}
		item.setId(id);
		return item;
	}

	public long addWord(String name) {
		ContentValues values = new ContentValues();
		values.put(Constants.WORD_NAME, name);
		db = getWritableDatabase();
		long id = 0;
		try {
			id = db.insertOrThrow(Constants.WORD_TABLE, null, values);
		} catch (SQLiteConstraintException ex) {
			Log.i(Constants.DATAHELPER_TAG, " Word Already Exist");
		} finally {
			db.close();
		}
		return id;
	}

	public long addAccount(String newName) {
		ContentValues values = new ContentValues();
		values.put(Constants.ACCOUNT_NAME, newName);
		db = getWritableDatabase();
		long id = 0;
		try {
			id = db.insertOrThrow(Constants.ACCOUNT_TABLE, null, values);
		} catch (SQLiteConstraintException ex) {
			Log.i(Constants.DATAHELPER_TAG, " Account Already Exist");
		} finally {
			db.close();
		}
		return id;
	}

	public List<String> getAccounts() {
		db = getReadableDatabase();
		Cursor accountsCur = db.query(Constants.ACCOUNT_TABLE,
				new String[] { Constants.ACCOUNT_NAME }, null, null, null,
				null, Constants.ACCOUNT_NAME + " ASC");
		List<String> accounts = new ArrayList<String>();
		try {
			if (accountsCur != null && accountsCur.moveToFirst()
					&& accountsCur.getCount() > 0) {
				do {
					accounts.add(accountsCur.getString(0));
				} while (accountsCur.moveToNext());
			}
		} catch (NullPointerException ex) {
			Log.e(Constants.DATAHELPER_TAG, "Cursor  " + ex.getMessage());
		} finally {
			accountsCur.close();
			db.close();
		}
		return accounts;
	}

	public String[] getWords() {
		db = getReadableDatabase();
		Cursor accountsCur = db.query(Constants.WORD_TABLE,
				new String[] { Constants.WORD_NAME }, null, null, null, null,
				Constants.WORD_NAME + " ASC");
		List<String> accounts = new ArrayList<String>();
		try {
			if (accountsCur != null && accountsCur.moveToFirst()
					&& accountsCur.getCount() > 0) {
				do {
					accounts.add(accountsCur.getString(0));
				} while (accountsCur.moveToNext());
			}
		} catch (NullPointerException ex) {
			Log.e(Constants.DATAHELPER_TAG, "Cursor  " + ex.getMessage());
		} finally {
			accountsCur.close();
			db.close();
		}
		String[] arrayWords = new String[accounts.size()];
		return accounts.toArray(arrayWords);
	}

	public List<Item> getAllItems(String selectedAccount) {
		db = getReadableDatabase();
		Cursor accountsCur = db.query(Constants.ITEM_TABLE, null,
				Constants.ITEM_ACCOUNT + "=?",
				new String[] { selectedAccount }, null, null,
				Constants.ITEM_DATE + " DESC");
		List<Item> accounts = new ArrayList<Item>();
		try {
			if (accountsCur != null && accountsCur.moveToFirst()
					&& accountsCur.getCount() > 0) {
				do {
					accounts.add(new Item(accountsCur.getLong(0), accountsCur
							.getString(1), accountsCur.getDouble(2), new Date(
							accountsCur.getLong(3)), accountsCur.getString(4)));
				} while (accountsCur.moveToNext());
			}
		} catch (NullPointerException ex) {
			Log.e(Constants.DATAHELPER_TAG, "Cursor  " + ex.getMessage());
		} finally {
			accountsCur.close();
			db.close();
		}

		return accounts;
	}

	public Item getItemById(long id) {
		db = getReadableDatabase();
		Item item = null;
		Cursor ItemCur = db.query(Constants.ITEM_TABLE, null, Constants.ITEM_ID
				+ "=?", new String[] { String.valueOf(id) }, null, null, null);
		try {
			if (ItemCur != null && ItemCur.moveToFirst()
					&& ItemCur.getCount() > 0) {
				item = new Item(ItemCur.getLong(0), ItemCur.getString(1),
						ItemCur.getDouble(2), new Date(ItemCur.getLong(3)),
						ItemCur.getString(4));
			}
		} catch (NullPointerException ex) {

		} finally {
			ItemCur.close();
			db.close();
		}
		return item;
	}

	public double getAmountCount(String selectedAccount) {
		db = getReadableDatabase();
		Cursor accountsCur = db.query(Constants.ITEM_TABLE,
				new String[] { Constants.ITEM_VALUE }, Constants.ITEM_ACCOUNT
						+ "=?", new String[] { selectedAccount }, null, null,
				null);
		double count = 0.0;
		try {
			if (accountsCur != null && accountsCur.moveToFirst()
					&& accountsCur.getCount() > 0) {
				do {
					count += accountsCur.getDouble(0);

				} while (accountsCur.moveToNext());
			}
		} catch (NullPointerException ex) {
			Log.e(Constants.DATAHELPER_TAG, "Cursor  " + ex.getMessage());
		} finally {
			accountsCur.close();
			db.close();
		}
		count += 0.001;
		StringBuilder s = new StringBuilder(count + "");
		count = Double.parseDouble(s.substring(0, s.indexOf(".") + 3));
		return count;
	}

	public double getTodayAmountCount(String selectedAccount) {
		db = getReadableDatabase();

		Cursor accountsCur = db.query(Constants.ITEM_TABLE, new String[] {
				Constants.ITEM_VALUE, Constants.ITEM_DATE },
				Constants.ITEM_ACCOUNT + "=?",
				new String[] { selectedAccount }, null, null,
				Constants.ITEM_DATE + " DESC");
		double count = 0.0;
		try {
			if (accountsCur != null && accountsCur.moveToFirst()
					&& accountsCur.getCount() > 0) {
				int date = 0;

				do {
					date = new Date(Long.parseLong(accountsCur.getString(1)))
							.getDate();
					if (new Date().getDate() == date) {
						count += accountsCur.getDouble(0);
					}

				} while (accountsCur.moveToNext());
			}
		} catch (NullPointerException ex) {
			Log.e(Constants.DATAHELPER_TAG, "Cursor  " + ex.getMessage());
		} finally {
			accountsCur.close();
			db.close();
		}
		count += 0.001;
		StringBuilder s = new StringBuilder(count + "");
		count = Double.parseDouble(s.substring(0, s.indexOf(".") + 3));

		return count;
	}

	public List<Item> getRecentItems(String selectedAccount) {
		Log.i("called", "again");
		List<Item> items = new ArrayList<Item>();
		List<Item> newItems = new ArrayList<Item>();

		items = getAllItems(selectedAccount);
		int today = new Date().getDate();
		for (Item item : items) {
			if (today == item.getDateTime().getDate()) {
				newItems.add(item);
			}
		}
		return newItems;
	}

	public int deleteItemById(long id) {
		int deleted = 0;
		db = getWritableDatabase();
		try {
			deleted = db.delete(Constants.ITEM_TABLE, Constants.ITEM_ID + "=?",
					new String[] { String.valueOf(id) });
		} finally {
			db.close();
		}
		return deleted;
	}

	public int deleteAccount(String selectedAccount) {
		db = getWritableDatabase();
		int deletedRows = 0;
		try {
			if (db.delete(Constants.ACCOUNT_TABLE, Constants.ACCOUNT_NAME
					+ "=?", new String[] { selectedAccount }) > 0) {
				deletedRows = db.delete(Constants.ITEM_TABLE,
						Constants.ITEM_ACCOUNT + "=?",
						new String[] { selectedAccount });
			}

		} finally {
			db.close();
		}
		return deletedRows;
	}

	public boolean addTask(Task task) {
		if (task.getRepeatId() > -1) {
			db = getWritableDatabase();
			ContentValues values = new ContentValues();
			Item item = task.getItem();
			values.put(Constants.ITEM_NAME, item.getName());
			values.put(Constants.ITEM_VALUE, item.getValue());
			values.put(Constants.ITEM_DATE, System.currentTimeMillis());
			values.put(Constants.ITEM_ACCOUNT, item.getAccountName());
			values.put(Constants.TASK_REPEAT_ID, task.getRepeatId());
			values.put(Constants.TASK_INSERTED_COUNT, task.getInsertedCount());
			try {
				long id = (long) task.getTaskId();
				if (id > 0) {
					if (db.update(Constants.TASK_TABLE, values,
							Constants.TASK_ID + "=?",
							new String[] { String.valueOf(id) }) > 0) {
						Log.i(Constants.DATAHELPER_TAG, "updated task, id:"
								+ id);
						return true;
					}
				} else {
					id = db.insertOrThrow(Constants.TASK_TABLE, null, values);
					Log.i("Task:", "id:" + id);
					return true;
				}

			} catch (SQLException ex) {
				Log.e(Constants.DATAHELPER_TAG, "addTask() " + ex.getMessage());
			} finally {
				db.close();
			}
		}
		return false;
	}

	public List<Task> getAllTasks() {
		List<Task> tasks = new ArrayList<Task>();
		db = getReadableDatabase();
		Cursor cursor = db.query(Constants.TASK_TABLE, null, null, null, null,
				null, Constants.ITEM_ACCOUNT);
		try {
			if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
				do {
					Task task = new Task(new Item(cursor.getLong(1),
							cursor.getString(2), cursor.getDouble(3), new Date(
									cursor.getLong(4)), cursor.getString(5)),
							cursor.getInt(6), cursor.getInt(7));
					task.setTaskId(cursor.getInt(0));
					tasks.add(task);

				} while (cursor.moveToNext());
			}
		} finally {
			cursor.close();
			db.close();
		}
		return tasks;
	}
}