package com.android.one;

public class Task {

	protected int taskId;
	protected Item item;
	protected int repeatId;
	protected int insertedCount;

	public Task(Item item, int repeatId, int insertedCount) {
		this.item = item;
		this.repeatId = repeatId;
		this.insertedCount = insertedCount;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public int getRepeatId() {
		return repeatId;
	}

	public void setRepeatId(int repeatId) {
		this.repeatId = repeatId;
	}

	public int getInsertedCount() {
		return insertedCount;
	}

	public void setInsertedCount(int insertedCount) {
		this.insertedCount = insertedCount;
	}

}
