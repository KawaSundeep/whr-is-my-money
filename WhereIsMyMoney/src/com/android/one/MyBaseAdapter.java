package com.android.one;

import java.util.ArrayList;
import java.util.List;

import com.android.one.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MyBaseAdapter extends BaseAdapter {

	public List<Item> results = new ArrayList<Item>();
	public Context context = null;

	public MyBaseAdapter(Context context, List<Item> results) {
		this.context = context;
		this.results = results;
	}

	@Override
	public int getCount() {
		return results.size();
	}

	@Override
	public Object getItem(int position) {
		return results.get(position);
	}

	@Override
	public long getItemId(int position) {
		return results.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh;

		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.recentlistrow, null);
			vh = new ViewHolder();

			vh.name = (TextView) convertView.findViewById(R.id.item_tv);
			vh.value = (TextView) convertView.findViewById(R.id.value_tv);

			convertView.setTag(vh);
		} else {
			vh = (ViewHolder) convertView.getTag();
		}

		vh.name.setText(results.get(position).getName());

		String value = results.get(position).getValue() + "";

		vh.value.setText(addZeroIfRequired(value));
		return convertView;
	}

	static class ViewHolder {
		TextView name;
		TextView value;
	}

	public String addZeroIfRequired(String value) {
		/*
		 * format the value for 2 digits after . example: we need an extra
		 * zero(0) after value 34.3 i.e, 34.30 but not after 34.35
		 */
		char isSeperator = value.charAt(value.length() - 2);
		if (isSeperator == '.') {
			value = value + "0";
		}
		return value;
	}

}
