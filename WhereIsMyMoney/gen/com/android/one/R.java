/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.android.one;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int boxes_background=0x7f020000;
        public static final int carbon=0x7f020001;
        public static final int ic_launcher=0x7f020002;
        public static final int polaroid_background=0x7f020003;
        public static final int space1=0x7f020004;
        public static final int space3=0x7f020005;
        public static final int space4=0x7f020006;
    }
    public static final class id {
        public static final int account_spnr1=0x7f06000c;
        public static final int account_spnr2=0x7f06000e;
        public static final int accountsList_lv=0x7f060001;
        public static final int autoComplete_tv=0x7f060006;
        public static final int createAccount_mi=0x7f060013;
        public static final int create_btn=0x7f060002;
        public static final int delete=0x7f060016;
        public static final int deleteAccount_mi=0x7f060014;
        public static final int edit=0x7f060015;
        public static final int itemDate_tv=0x7f060005;
        public static final int itemName_tv=0x7f060003;
        public static final int itemValue_tv=0x7f060004;
        public static final int item_tv=0x7f060011;
        public static final int listCount_tv=0x7f06000f;
        public static final int myList_lv=0x7f060010;
        public static final int newAccount_et=0x7f060000;
        public static final int recentList_lv=0x7f06000b;
        public static final int repeat_btn=0x7f060008;
        public static final int save_btn=0x7f060009;
        public static final int spent_tv=0x7f06000a;
        public static final int value_et=0x7f060007;
        public static final int value_tv=0x7f060012;
        public static final int view_items=0x7f06000d;
    }
    public static final class layout {
        public static final int account=0x7f030000;
        public static final int autocompleterow=0x7f030001;
        public static final int completelistrow=0x7f030002;
        public static final int main=0x7f030003;
        public static final int mylistlayout=0x7f030004;
        public static final int recentlistrow=0x7f030005;
    }
    public static final class menu {
        public static final int account_menu=0x7f050000;
        public static final int listitemmenu=0x7f050001;
    }
    public static final class string {
        public static final int account=0x7f04000b;
        public static final int app_name=0x7f040001;
        public static final int create_account=0x7f04000c;
        public static final int delete_account=0x7f04000d;
        public static final int delete_from_list=0x7f04000a;
        public static final int edit=0x7f040009;
        public static final int hello=0x7f040000;
        public static final int item=0x7f040002;
        public static final int item_hint=0x7f040003;
        public static final int repeat=0x7f04000f;
        public static final int save=0x7f040006;
        public static final int spent=0x7f040008;
        public static final int spentToday=0x7f04000e;
        public static final int value=0x7f040004;
        public static final int value_hint=0x7f040005;
        public static final int view_items=0x7f040007;
    }
}
